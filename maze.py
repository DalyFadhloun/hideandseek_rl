import numpy as np
import random
import pygame
import sys

# Define constants for the graphical interface
SCREEN_WIDTH, SCREEN_HEIGHT = 400, 400
GRID_SIZE = (5, 5)
CELL_SIZE = SCREEN_WIDTH // GRID_SIZE[0]
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

# Initialize Pygame
pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Treasure Hunt Maze")

# Define the possible actions (up, down, left, right)
ACTIONS = [(0, -1), (0, 1), (-1, 0), (1, 0)]

# Define the reward values
REWARD_TREASURE = 10
REWARD_TRAP = -5
REWARD_STEP = -1

# Define the exploration-exploitation trade-off (epsilon-greedy)
EPSILON = 0.1

# Define the learning rate and discount factor
LEARNING_RATE = 0.1
DISCOUNT_FACTOR = 0.9

# Initialize the Q-table with zeros
Q_table = np.zeros((GRID_SIZE[0], GRID_SIZE[1], len(ACTIONS)))

# Define the starting and goal positions
start_position = (0, 0)
goal_position = (GRID_SIZE[0] - 1, GRID_SIZE[1] - 1)

# Function to draw the grid, agent, and objects
def draw_grid():
    screen.fill(WHITE)
    for row in range(GRID_SIZE[0]):
        for col in range(GRID_SIZE[1]):
            rect = pygame.Rect(col * CELL_SIZE, row * CELL_SIZE, CELL_SIZE, CELL_SIZE)
            pygame.draw.rect(screen, BLUE, rect, 1)
            if (row, col) == goal_position:
                pygame.draw.circle(screen, GREEN, rect.center, CELL_SIZE // 4)
            elif (row, col) in [(1, 1), (2, 2), (3, 3)]:
                pygame.draw.rect(screen, RED, rect)

    agent_rect = pygame.Rect(
        current_position[1] * CELL_SIZE,
        current_position[0] * CELL_SIZE,
        CELL_SIZE,
        CELL_SIZE,
    )
    pygame.draw.circle(screen, RED, agent_rect.center, CELL_SIZE // 4)

    # Display text if the agent touches a trap
    if current_position in [(1, 1), (2, 2), (3, 3)]:
        font = pygame.font.Font(None, 36)
        text = font.render("Agent touched a trap!", True, (255, 0, 0))
        text_rect = text.get_rect(center=(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2))
        screen.blit(text, text_rect)

    pygame.display.flip()

# Initialize tracking variables
episode_rewards = []  # To store rewards for each episode
successful_episodes = 0  # To count the number of successful episodes

# Main game loop
NUM_EPISODES = 100
for episode in range(NUM_EPISODES):
    current_position = start_position
    episode_reward = 0  # To track the cumulative reward for this episode

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

    while current_position != goal_position:
        # Exploration-exploitation trade-off
        if random.uniform(0, 1) < EPSILON:
            action = random.choice(range(len(ACTIONS)))  # Explore
        else:
            action = np.argmax(Q_table[current_position[0], current_position[1]])  # Exploit

        # Perform the selected action
        new_position = (
            current_position[0] + ACTIONS[action][0],
            current_position[1] + ACTIONS[action][1],
        )

        # Check if the agent hits a wall and stay in the same position if it does
        if (
            0 <= new_position[0] < GRID_SIZE[0]
            and 0 <= new_position[1] < GRID_SIZE[1]
        ):
            next_position = new_position
        else:
            next_position = current_position

        # Calculate the reward
        if next_position == goal_position:
            reward = REWARD_TREASURE
        elif next_position in [(1, 1), (2, 2), (3, 3)]:
            reward = REWARD_TRAP
        else:
            reward = REWARD_STEP

        # Update the Q-value using the Bellman equation
        Q_table[current_position[0], current_position[1], action] = (
            1 - LEARNING_RATE
        ) * Q_table[current_position[0], current_position[1], action] + LEARNING_RATE * (
            reward
            + DISCOUNT_FACTOR
            * np.max(Q_table[next_position[0], next_position[1]])
        )

        # Move to the next state
        current_position = next_position

        # Update the cumulative reward for this episode
        episode_reward += reward

        # Update the graphical interface and insights
        draw_grid()
        pygame.time.delay(200)  # Delay for visualization

    # Episode is complete; store episode reward and check if successful
    episode_rewards.append(episode_reward)
    if current_position == goal_position:
        successful_episodes += 1

# Calculate and display insights
average_reward = sum(episode_rewards) / NUM_EPISODES
success_rate = (successful_episodes / NUM_EPISODES) * 100

print(f"Average Reward: {average_reward}")
print(f"Success Rate: {success_rate}%")

# After training, you can use the Q-table to make decisions in the maze
pygame.quit()
