import gym
import numpy as np
import random
import pygame
import sys

# Constants
WIDTH, HEIGHT = 400, 400
GRID_SIZE = 5
CELL_SIZE = WIDTH // GRID_SIZE
FPS = 10
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
GRAY = (128, 128, 128)
BLUE = (0, 0, 255)

# Initialize Pygame
pygame.init()
font = pygame.font.Font(None, 36)
screen = pygame.display.set_mode((WIDTH, HEIGHT))
clock = pygame.time.Clock()

# Reward Functions
def calculate_reward_for_hider(current, next_, seeker):
    if next_ == seeker:
        return -10
    return 1

def calculate_reward_for_seeker(current, next_, hider):
    if next_ == hider:
        return 10
    return -1

# Draw grid
def draw_grid():
    for x in range(0, WIDTH, CELL_SIZE):
        pygame.draw.line(screen, GRAY, (x, 0), (x, HEIGHT))
    for y in range(0, HEIGHT, CELL_SIZE):
        pygame.draw.line(screen, GRAY, (0, y), (WIDTH, y))

# Define the hide and seek environment
class HideAndSeekEnv(gym.Env):
    def __init__(self):
        self.hider_position = [0, 0]
        self.seeker_position = [GRID_SIZE-1, GRID_SIZE-1]

    def reset(self):
        self.hider_position = [random.randint(0, GRID_SIZE-1), random.randint(0, GRID_SIZE-1)]
        self.seeker_position = [random.randint(0, GRID_SIZE-1), random.randint(0, GRID_SIZE-1)]

    def step(self, hider_action, seeker_action):
        self.move(self.hider_position, hider_action)
        self.move(self.seeker_position, seeker_action)

    def move(self, position, action):
        if action == 0:
            position[1] = max(0, position[1] - 1)
        elif action == 1:
            position[1] = min(GRID_SIZE-1, position[1] + 1)
        elif action == 2:
            position[0] = max(0, position[0] - 1)
        elif action == 3:
            position[0] = min(GRID_SIZE-1, position[0] + 1)
        elif action == 4:
            position[0] = min(GRID_SIZE-1, position[0] + 1)
            position[1] = min(GRID_SIZE-1, position[1] + 1)

# Hider Agent Class
class HiderAgent:
    def __init__(self, num_actions):
        self.q_table = np.random.uniform(low=-1, high=1, size=(GRID_SIZE, GRID_SIZE, num_actions))

    def select_action(self, state, epsilon=0.1):
        if np.random.rand() < epsilon:
            return np.random.randint(0, 4)  # 4 actions
        else:
            return np.argmax(self.q_table[state[1], state[0]])

    def train(self, state, action, reward, next_state, alpha=0.1, gamma=0.99):
        old_value = self.q_table[state[1], state[0], action]
        next_max = np.max(self.q_table[next_state[1], next_state[0]])

        new_value = (1 - alpha) * old_value + alpha * (reward + gamma * next_max)
        self.q_table[state[1], state[0], action] = new_value

# Seeker Agent Class
class SeekerAgent:
    def __init__(self, num_actions):
        self.q_table = np.random.uniform(low=-1, high=1, size=(GRID_SIZE, GRID_SIZE, num_actions))

    def select_action(self, state, epsilon=0.1):
        if np.random.rand() < epsilon:
            return np.random.randint(0, 4)  # 4 actions
        else:
            return np.argmax(self.q_table[state[1], state[0]])

    def train(self, state, action, reward, next_state, alpha=0.1, gamma=0.99):
        old_value = self.q_table[state[1], state[0], action]
        next_max = np.max(self.q_table[next_state[1], next_state[0]])

        new_value = (1 - alpha) * old_value + alpha * (reward + gamma * next_max)
        self.q_table[state[1], state[0], action] = new_value

# Environment Initialization
env = HideAndSeekEnv()

# Agents Initialization
num_actions = 5
hider_agent = HiderAgent(num_actions)
seeker_agent = SeekerAgent(num_actions)

# Training loop
num_episodes = 1000

for episode in range(num_episodes):
    env.reset()
    done = False
    caught = False

    while not done:
        screen.fill(WHITE)
        draw_grid()
        pygame.draw.circle(screen, GREEN, (env.hider_position[0]*CELL_SIZE + CELL_SIZE//2, env.hider_position[1]*CELL_SIZE + CELL_SIZE//2), CELL_SIZE//2)
        pygame.draw.circle(screen, RED, (env.seeker_position[0]*CELL_SIZE + CELL_SIZE//2, env.seeker_position[1]*CELL_SIZE + CELL_SIZE//2), CELL_SIZE//2)

        if caught:
            text = font.render('Caught!', True, BLUE)
            screen.blit(text, (WIDTH//2 - text.get_width() // 2, HEIGHT//2 - text.get_height() // 2))
            pygame.display.update()
            pygame.time.wait(3000)
            done = True
            continue

        pygame.display.update()

        # Taking actions
        hider_state = env.hider_position.copy()
        seeker_state = env.seeker_position.copy()

        hider_action = hider_agent.select_action(hider_state, epsilon=0.1)
        seeker_action = seeker_agent.select_action(seeker_state, epsilon=0.1)

        env.step(hider_action, seeker_action)

        next_hider_state = env.hider_position.copy()
        next_seeker_state = env.seeker_position.copy()

        reward_hider = calculate_reward_for_hider(hider_state, next_hider_state, seeker_state)
        reward_seeker = calculate_reward_for_seeker(seeker_state, next_seeker_state, hider_state)

        hider_agent.train(hider_state, hider_action, reward_hider, next_hider_state)
        seeker_agent.train(seeker_state, seeker_action, reward_seeker, next_seeker_state)

        if next_hider_state == next_seeker_state:
            caught = True

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        clock.tick(FPS)

pygame.quit()
