import pygame
import math
import random

# Initialize Pygame
pygame.init()

# Constants
WIDTH, HEIGHT = 800, 600
TARGET_RADIUS = 20
ROCKET_RADIUS = 10
TARGET_COLOR = (255, 0, 0)
ROCKET_COLOR = (0, 0, 255)
BACKGROUND_COLOR = (0, 0, 0)
FPS = 60

# Initialize the screen
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Guided Rocket Simulation")

# Rocket properties
rocket_x, rocket_y = WIDTH // 2, HEIGHT // 2
rocket_speed = 2
rocket_angle = math.pi / 4  # Initial angle

# Target properties
target_x, target_y = WIDTH // 2, HEIGHT // 2  # Initial position of the target
target_radius = 200  # Radius of the circular path
target_speed = 0.02  # Angular speed (radians per frame)

# Proportional controller gain
kp = 0.02

# Main loop
clock = pygame.time.Clock()
running = True
target_reached = False

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    if not target_reached:
        # Update the target's position in a circular path
        target_x = WIDTH // 2 + target_radius * math.cos(target_speed)
        target_y = HEIGHT // 2 + target_radius * math.sin(target_speed)
        target_speed += 0.02  # Increase the angular speed for continuous movement

        # Calculate the angle to the target
        angle_to_target = math.atan2(target_y - rocket_y, target_x - rocket_x)

        # Predict the target's future position based on its current speed
        target_speed_x = target_radius * math.cos(target_speed)
        target_speed_y = target_radius * math.sin(target_speed)
        target_future_x = target_x + target_speed_x
        target_future_y = target_y + target_speed_y

        # Calculate the angle to the future target position (Proportional Control)
        future_angle = math.atan2(target_future_y - rocket_y, target_future_x - rocket_x)

        # Adjust the rocket's angle towards the future target position
        rocket_angle += kp * (future_angle - rocket_angle)

        # Move the rocket
        rocket_x += rocket_speed * math.cos(rocket_angle)
        rocket_y += rocket_speed * math.sin(rocket_angle)

        # Check if the rocket has reached the predicted target position
        distance_to_target = math.sqrt((rocket_x - target_future_x) ** 2 + (rocket_y - target_future_y) ** 2)
        if distance_to_target < TARGET_RADIUS + ROCKET_RADIUS:
            target_reached = True

    # Clear the screen
    screen.fill(BACKGROUND_COLOR)

    # Draw the target
    pygame.draw.circle(screen, TARGET_COLOR, (int(target_x), int(target_y)), TARGET_RADIUS)

    if target_reached:
        # Display "Target Reached" and reset
        font = pygame.font.Font(None, 36)
        text = font.render("Target Reached", True, (255, 255, 255))
        text_rect = text.get_rect(center=(WIDTH // 2, HEIGHT // 2))
        screen.blit(text, text_rect)

        # Reset the target
        target_x = random.randint(TARGET_RADIUS, WIDTH - TARGET_RADIUS)
        target_y = random.randint(TARGET_RADIUS, HEIGHT - TARGET_RADIUS)

        # Reset the rocket's position and angle
        rocket_x, rocket_y = WIDTH // 2, HEIGHT // 2
        rocket_angle = math.pi / 4
        target_reached = False

    else:
        # Draw the rocket
        pygame.draw.circle(screen, ROCKET_COLOR, (int(rocket_x), int(rocket_y)), ROCKET_RADIUS)

    # Update the display
    pygame.display.flip()

    # Control the frame rate
    clock.tick(FPS)

# Quit Pygame
pygame.quit()
